import { createAvatar } from '@dicebear/core';
import { lorelei } from '@dicebear/collection';
import { schema } from '@dicebear/core';


const seed = 'John Doe';

const avatar = createAvatar(lorelei, {
    seed: seed,
    // ... other options
});

console.log("avatar: " );
console.log(avatar);
/*{
  toString: [Function: toString],
  toJson: [Function: toJson],
  toDataUriSync: [Function: toDataUriSync],
  toDataUri: [Function: toDataUri],
  toFile: [Function: toFile],
  toArrayBuffer: [Function: toArrayBuffer],
  png: [Function: png],
  jpeg: [Function: jpeg]
}
*/
console.log("avatar.toString: \n" +avatar.toString())
// 或者写入到文件中
// require('fs').writeFileSync('avatar.svg', svg);

// 合并对象
const options = {
    ...schema.properties,
    ...lorelei.schema.properties,
};
console.log("schema.properties: " );
console.log(schema.properties);
/*
* {
  seed: { type: 'string' },
  flip: { type: 'boolean', default: false },
  rotate: { type: 'integer', minimum: 0, maximum: 360, default: 0 },
  scale: { type: 'integer', minimum: 0, maximum: 200, default: 100 },
  radius: { type: 'integer', minimum: 0, maximum: 50, default: 0 },
  size: { type: 'integer', minimum: 1 },
  backgroundColor: {
    type: 'array',
    items: { type: 'string', pattern: '^(transparent|[a-fA-F0-9]{6})$' }
  },
  backgroundType: {
    type: 'array',
    items: { type: 'string', enum: [Array] },
    default: [ 'solid' ]
  },
  backgroundRotation: {
    type: 'array',
    items: { type: 'integer', minimum: -360, maximum: 360 },
    default: [ 0, 360 ]
  },
  translateX: { type: 'integer', minimum: -100, maximum: 100, default: 0 },
  translateY: { type: 'integer', minimum: -100, maximum: 100, default: 0 },
  clip: { type: 'boolean', default: true },
  randomizeIds: { type: 'boolean', default: false }
}

* */
console.log("lorelei.schema.properties: " );
console.log(lorelei.schema.properties);
/*
* {
  beard: {
    type: 'array',
    items: { type: 'string', enum: [Array] },
    default: [ 'variant01', 'variant02' ]
  },
  beardProbability: { type: 'integer', minimum: 0, maximum: 100, default: 5 },
  earrings: {
    type: 'array',
    items: { type: 'string', enum: [Array] },
    default: [ 'variant01', 'variant02', 'variant03' ]
  },
  earringsColor: {
    type: 'array',
    items: { type: 'string', pattern: '^(transparent|[a-fA-F0-9]{6})$' },
    default: [ '000000' ]
  },
  earringsProbability: { type: 'integer', minimum: 0, maximum: 100, default: 10 },
  eyebrows: {
    type: 'array',
    items: { type: 'string', enum: [Array] },
    default: [
      'variant13', 'variant12',
      'variant11', 'variant10',
      'variant09', 'variant08',
      'variant07', 'variant06',
      'variant05', 'variant04',
      'variant03', 'variant02',
      'variant01'
    ]
  },
  eyebrowsColor: {
    type: 'array',
    items: { type: 'string', pattern: '^(transparent|[a-fA-F0-9]{6})$' },
    default: [ '000000' ]
  },
  eyes: {
    type: 'array',
    items: { type: 'string', enum: [Array] },
    default: [
      'variant24', 'variant23',
      'variant22', 'variant21',
      'variant20', 'variant19',
      'variant18', 'variant17',
      'variant16', 'variant15',
      'variant14', 'variant13',
      'variant12', 'variant11',
      'variant10', 'variant09',
      'variant08', 'variant07',
      'variant06', 'variant05',
      'variant04', 'variant03',
      'variant02', 'variant01'
    ]
  },
  eyesColor: {
    type: 'array',
    items: { type: 'string', pattern: '^(transparent|[a-fA-F0-9]{6})$' },
    default: [ '000000' ]
  },
  freckles: {
    type: 'array',
    items: { type: 'string', enum: [Array] },
    default: [ 'variant01' ]
  },
  frecklesColor: {
    type: 'array',
    items: { type: 'string', pattern: '^(transparent|[a-fA-F0-9]{6})$' },
    default: [ '000000' ]
  },
  frecklesProbability: { type: 'integer', minimum: 0, maximum: 100, default: 5 },
  glasses: {
    type: 'array',
    items: { type: 'string', enum: [Array] },
    default: [ 'variant01', 'variant02', 'variant03', 'variant04', 'variant05' ]
  },
  glassesColor: {
    type: 'array',
    items: { type: 'string', pattern: '^(transparent|[a-fA-F0-9]{6})$' },
    default: [ '000000' ]
  },
  glassesProbability: { type: 'integer', minimum: 0, maximum: 100, default: 10 },
  hair: {
    type: 'array',
    items: { type: 'string', enum: [Array] },
    default: [
      'variant48', 'variant47', 'variant46',
      'variant45', 'variant44', 'variant43',
      'variant42', 'variant41', 'variant40',
      'variant39', 'variant38', 'variant37',
      'variant36', 'variant35', 'variant34',
      'variant33', 'variant32', 'variant31',
      'variant30', 'variant29', 'variant28',
      'variant27', 'variant26', 'variant25',
      'variant24', 'variant23', 'variant22',
      'variant21', 'variant20', 'variant19',
      'variant18', 'variant17', 'variant16',
      'variant15', 'variant14', 'variant13',
      'variant12', 'variant11', 'variant10',
      'variant09', 'variant08', 'variant07',
      'variant06', 'variant05', 'variant04',
      'variant03', 'variant02', 'variant01'
    ]
  },
  hairAccessories: {
    type: 'array',
    items: { type: 'string', enum: [Array] },
    default: [ 'flowers' ]
  },
  hairAccessoriesColor: {
    type: 'array',
    items: { type: 'string', pattern: '^(transparent|[a-fA-F0-9]{6})$' },
    default: [ '000000' ]
  },
  hairAccessoriesProbability: { type: 'integer', minimum: 0, maximum: 100, default: 5 },
  hairColor: {
    type: 'array',
    items: { type: 'string', pattern: '^(transparent|[a-fA-F0-9]{6})$' },
    default: [ '000000' ]
  },
  head: {
    type: 'array',
    items: { type: 'string', enum: [Array] },
    default: [ 'variant04', 'variant03', 'variant02', 'variant01' ]
  },
  mouth: {
    type: 'array',
    items: { type: 'string', enum: [Array] },
    default: [
      'happy01', 'happy02', 'happy03',
      'happy04', 'happy05', 'happy06',
      'happy07', 'happy08', 'happy18',
      'happy09', 'happy10', 'happy11',
      'happy12', 'happy13', 'happy14',
      'happy17', 'happy15', 'happy16',
      'sad01',   'sad02',   'sad03',
      'sad04',   'sad05',   'sad06',
      'sad07',   'sad08',   'sad09'
    ]
  },
  mouthColor: {
    type: 'array',
    items: { type: 'string', pattern: '^(transparent|[a-fA-F0-9]{6})$' },
    default: [ '000000' ]
  },
  nose: {
    type: 'array',
    items: { type: 'string', enum: [Array] },
    default: [
      'variant01',
      'variant02',
      'variant03',
      'variant04',
      'variant05',
      'variant06'
    ]
  },
  noseColor: {
    type: 'array',
    items: { type: 'string', pattern: '^(transparent|[a-fA-F0-9]{6})$' },
    default: [ '000000' ]
  },
  skinColor: {
    type: 'array',
    items: { type: 'string', pattern: '^(transparent|[a-fA-F0-9]{6})$' },
    default: [ 'ffffff' ]
  }
}
*/
