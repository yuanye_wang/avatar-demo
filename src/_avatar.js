import { createAvatar } from '@dicebear/core';
import { lorelei } from '@dicebear/collection';
import { schema } from '@dicebear/core';
// 创建头像的js文件

// 创建简单头像(测试用)
export default function createSimpleAvatar() {
    const seed = 'wangyy';
    let svg = createAvatar(lorelei, {
        "seed" : seed
    });
    return svg.toString();
}

/**
 * 
 * @param {*} type @dicebear/collection中的类型
 * @param {*} svgOptions 
 * @returns 
 */
function _createAvatar(type, svgOptions ) {
    const svg = createAvatar(type, svgOptions)
    return svg;
}

// 合并对象
const options = {
    ...schema.properties,
    ...lorelei.schema.properties,
  };

  console.log("schema.properties\n")
  console.log(schema.properties)
  console.log("lorelei.schema.properties\n")
  console.log(lorelei.schema.properties)



