import createSimpleAvatar from './_avatar.js';
import _ from 'lodash';



function component() {
    const element = document.createElement('div');
    const svg = createSimpleAvatar();
    element.innerHTML = svg;
    return element;
  }
 
  document.body.appendChild(component());