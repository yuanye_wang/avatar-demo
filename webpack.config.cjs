// webpack.config.cjs
const path = require('path');

module.exports = {
  // 入口起点 当前目录需要./开头
  entry: './src/index.js',

  // 输出配置
  output: {
    path: path.resolve(__dirname, 'dist'), // 输出路径
    filename: 'main.js', // 打包后输出的文件名
  },
  // 开发服务器配置（可选）
  devServer: {
    static: path.join(__dirname, 'dist'), // 服务器提供的静态文件基础目录
    compress: true,
    historyApiFallback: true,
    port: 3000,
  },

  // 加载器配置
  module: {
    rules: [
      {
        test: /\.js$/, // 匹配.js文件
        exclude: /node_modules/, // 排除node_modules下的文件
        use: {
          loader: 'babel-loader', // 使用Babel转换ES6+语法
          options: {
            presets: ['@babel/preset-env'],
          },
        },
      },
      // 添加其他加载器，例如处理CSS或图片等资源
      // ...
    ],
  },

  // 插件配置（可选）
  plugins: [
    // ... 这里可以添加各种插件，例如：
    // new HtmlWebpackPlugin() 生成HTML文件
    // new MiniCssExtractPlugin() 提取CSS为独立文件
  ],
};
