import express from 'express';
const app = express();
const port = 3000;
// 使用express作为服务器

// 设置静态文件目录
app.use(express.static('public'));
app.get('/', (req, res) => {
    res.send('Hello from the server!');
});

app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});
